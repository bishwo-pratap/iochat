
var express 		= require('express'),
	app 			= express(),
	server 			= require('http').createServer(app),
	io 				= require('socket.io').listen(server),	
	bodyParser 		= require('body-parser');
	mongoose  		= require('mongoose'),
	authController  = require('./js/authentication-controller.js'),
	chatController  = require('./js/chat-controller.js'),

	connections 	= [],
	UserArr			= [],
	users 			= {};

app.use('/views', express.static(__dirname+'/views'));
app.use('/js', express.static(__dirname+'/js'));
app.use('/css', express.static(__dirname+'/css'));
app.use('/node_modules', express.static(__dirname+'/node_modules'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect("mongodb://localhost:27017/MEANSocial");


app.get('/', function(req, res){
	res.sendFile(__dirname+'/views/index.html');
});

app.post('/api/user/login', authController.login);
app.post('/api/chat/getConversationDetails', chatController.getChatLog);



io.sockets.on('connection', function(socket){
	connections.push(socket);
	console.log('Connected: %s sockets connected', connections.length);

	function updateUsernames(){
		console.log("Inside update username");

		io.sockets.emit("get users", UserArr);
	};

	socket.on('disconnect', function(data){
		delete users[socket.username];
		// users.splice(users.indexOf(socket.username),1);
		connections.splice(connections.indexOf(socket),1);
		UserArr.splice(UserArr.indexOf(socket.username),1);
		updateUsernames();
		console.log('Disconnected: %s sockets connected', connections.length);

	});

	socket.on('send message', function(data){
		if(data){
			var chatting_with = data.chatting_with,
				own_name 	  = data.own_name;
			delete data[chatting_with];
			delete data[own_name];
			var chatSaved = chatController.postChat(data);
			if(chatSaved){
				users[own_name].emit('new message',{msg: data.msg , user: socket.username, from: own_name});
				users[chatting_with].emit('new message',{msg: data.msg , user: socket.username, from:own_name});
			}
		}
	});

	socket.on('new user', function(data, callback){
		if(data.email in users){
			callback(false);
		} else {
			callback(true);
			socket.username = data.email;
			UserArr.push({username: socket.username, id: data.id});
			users[socket.username] = socket;
			updateUsernames();
		}
	});

});

server.listen(3000);
console.log("3000");