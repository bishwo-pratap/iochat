var mongoose = require('mongoose');

var ChatSchema = mongoose.Schema({
	from: {
		type: String,
		required: true
	},
	to: { 
		type: String,
		required: true
	},
	msg: {
		type: String,
		required: true
	},
    chatlog_id: {
		type: String,
		required: true
	},
	time: {
		type: Date, 
		default: Date.now
	}
});

ChatSchema.set('collection','Chat');

var Chat = module.exports = mongoose.model('Chat',ChatSchema);
