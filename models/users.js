var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	image: {
		type: String,
		required: false
	},
	username:{
		type:String,
		required: false
	},
	bio:{
		type:String,
		required: false
	}
});

UserSchema.set('collection','Users');
        
var Users = module.exports = mongoose.model('Users',UserSchema);