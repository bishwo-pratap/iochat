var mongoose = require('mongoose');

var ChatLogSchema = mongoose.Schema({
	chatlog_id:{
		type: mongoose.Schema.ObjectId,
		required: true
	},
	from: {
		type: String,
		required: true
	},
	to: { 
		type: String,
		required: true
	}
});

ChatLogSchema.set('collection','ChatLog');

var ChatLog = module.exports = mongoose.model('ChatLog',ChatLogSchema);