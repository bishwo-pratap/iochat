
var mongoose = require('mongoose');

var ChatLog = require('../models/chatLog');
var Chat = require('../models/chats');

module.exports = {
	getChatLog:function(req,res){
		var params = req.body,
			cond = {
				from: params.from,
				to: params.to
			},
			reverseCondition = {
				to: params.from,
				from: params.to
			};
		ChatLog.find({$or: [cond, reverseCondition]},function(err, data){
			if(err) throw err;
			if(data.length>0 && data.length == 1){
				var chatlog_id = data[0]._id;

				Chat.find({chatlog_id:chatlog_id}, function(err, data){
					if(err) throw err;
					if(data.length > 0){
						res.json({
							data : data
						});
						return;
					}
					res.json({
						msg: "no chat"
					});
					return;
				}).sort({'time': 1}).limit(20);
			} else {
				console.log("Chat Log Not Found... Inserting Chat Log");
				var chatlog = new ChatLog(cond);
				chatlog.save();
				res.json({
					msg: "new msg log saved"
				});
				return;
			}
			console.log(data);
		})

	},

    postChat: function(req,res){
        console.log('inside postChat');
        var chat = new Chat(req);
        if(chat.save()){
        	return true;
        }

        //console.log(req.body);
    }
}