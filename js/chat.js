var	$ownId = $chatlogId = '';

$(document).ready(function(){
	var socket 	   = io.connect(),
		$msgForm   = $("#messageForm"),
		$msg 	   = $("#message"),
		$chat      = $(".chat"),
		$users     = $("#users"),
		$msgArea   = $("#messageArea"),
		$loginArea = $("#loginArea"),
		$loginForm = $("#loginForm"),
		$username  = $("#username"),
		$password  = $("#password"),
		chattingWith = chattingWithName = undefined;		

	socket.on('new message', function(data){
		var username  = data.user,
			from 	  = data.from, 
			alignment = bg = name = '';
	if(from == chattingWith || username == $username.val()){
			name  = ($username.val() == username) ? "You" : username;
			alignment = ($username.val() == username) ? 'text-right' : 'text-left';
			bg = ($username.val() == username) ? 'text-primary' : 'text-danger';
			$chat.append("<div class='well "+bg+" "+alignment+"'>"+"<b>"+name+":</b>"+data.msg+"</div>");
		}
	});

	$loginForm.submit(function(e){
		e.preventDefault();
		var data ={
			email : $username.val(),
			password : $password.val()
		};

		$.ajax({
			type: "POST",
			url: "/api/user/login",
			data: data
		})
		.done(function(data){
			socket.emit('new user', data , function(data){
				if(data){
					$loginArea.hide();
					$msgArea.show();
				}
			});
		});
	});

// 

	socket.on('get users', function(data){
		var html = '';
		for (var i = 0; i< data.length; i++) {
			var user = data[i].username,
				id 		 = data[i].id,
				username ='';
			username = ($username.val() == user) ? "You" : user;
			if(!$ownId)
				$ownId 	 = ($username.val() == user) ? id : null;
			html += "<li class='list-group-item user "+user+"' style='background-color:grey;' id='"+id+"'><span class='glyphicon glyphicon-one-fine-green-dot'></span>"+username+"</li>"
		}
		$users.html(html);
	});


	$("#users").on('click','.user',function(event){
		var userId = this.getAttribute("id"),
			userClass = this.getAttribute("class"),
			user = userClass.split(' ')[2];
		if(user != $username.val()){
			chattingWith = userId;
			chattingWithName = user;		

			//clear the chat div
			$chat.empty();
			var data = {
				from : $ownId,
				to 	 : chattingWith
			}
			$.ajax({
				type: "POST",
				data: data,
				url: '/api/chat/getConversationDetails'
			})
			.done(function(data){
				if(data.msg == "not chat" || data.msg == "new msg log saved"){
					$chat.append("<div class='well text-info text-center'>"+"<b>Start a chat with "+user+"</b></div>");
				}else{
					var data  = data.data;
					$chatlogId = data[0].chatlog_id;
					for(index in data){
						var	from  = data[index].from,
							msg   = data[index].msg,
							alignment = bg = name = '';
							// debugger;
						name  = (from != chattingWith) ? "You" : user;
						alignment = (from != chattingWith) ? 'text-right' : 'text-left';
						bg = (from != chattingWith) ? 'text-primary' : 'text-danger';
								$chat.append("<div class='well "+bg+" "+alignment+"'>"+"<b>"+name+":</b>"+msg+"</div>");
						
					}
				}

			}); 
		}
	});

	$('#message').keypress(function (e) {
	  	// e.preventDefault();
		 var key = e.which;
		 if(key == 13)  // the enter key code
		  {
			if(!chattingWith){
				alert('Select a user to start chat.');
				return;
			}

			if($msg.val().trim().length > 0){
				var data = {
						msg  : $msg.val(),
						from : $ownId,
						to 	 : chattingWith,
						chatlog_id : $chatlogId,
						chatting_with: chattingWithName,
						own_name: $username.val()
					}
				socket.emit('send message', data);
				$msg.val('');
				
			}
		  }
		}); 

});