var mongoose = require('mongoose');

var User = require('../models/users');


module.exports = {
	login: function(req, res){
		console.log("Inside Authentication Controller");

		User.find(req.body, function(err, result){
			
			if(err){
				console.log(err);
			}
			else if(result && result.length === 1){
				console.log("User Found::\n"+result);
				var userData = result[0],
					resp 	 = {
						id: userData._id,
						email: userData.email
					};
				res.json(resp);
			}
		});
	}

};